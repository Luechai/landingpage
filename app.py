import json
import os
import gitlab
import time
from datetime import datetime
from json2html import *
import os, io, zipfile, time

from flask import send_from_directory
from flask import Flask, render_template,request  # add request  16 Mar
import requests  #20 Mar
from zipfile import ZipFile
from flask import send_file, make_response
#pip install wget


from flask import Response

from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage
#ref >> https://stackoverflow.com/questions/61628503/flask-uploads-importerror-cannot-import-name-secure-filename

#18 Mar
from http import HTTPStatus


report = {}
report['Project'] = []

static_file_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'static')

app = Flask(__name__)
# app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
FILEPATH = '/'



APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLD = 'upload'
UPLOAD_FOLDER = os.path.join(APP_ROOT, UPLOAD_FOLD)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

ALLOWED_EXTENSIONS = {'txt','json'}
app.config['MAX_CONTENT_LENGTH'] = 100000 * 1024 * 1024
#app.config['MAX_CONTENT_PATH'] = 64 * 1024 * 1024

#scheduler = APScheduler()
#scheduler.init_app(app)
                
def CalAge(startdate):
    s = startdate.split('T')[0]
    startdate = datetime.strptime(s, '%Y-%m-%d')
    enddate = datetime.now() 

    time_difference = enddate - startdate
    age = time_difference.days
    return(age)


prjlen = len(report['Project'])

def funcZIP():
    zipObj = ZipFile('download.zip', 'w')
    #Add multiple files to the zip
    #windows
    zipObj.write('instance\\upload\\topics.txt')
    zipObj.write('static\\thereport.html')  
    #linux
    # zipObj.write('instance/upload/topics.txt')
    # zipObj.write('static/thereport.html') 
 
    zipObj.close()
    return "Already ZIP"
    
# def upload_form():
# 	return render_template('download.html')


@app.route('/')
def home():

    
    #zipObj = ZipFile('download.zip', 'w')
    #topics = eval(open("instance\upload\topics.txt",encoding="utf8").read()) # not work on linux 
    #in linux 
    # topics = eval(open("instance/upload/topics.txt",encoding="utf8").read()) 
    topics = eval(open("instance\\upload\\topics.txt",encoding="utf8").read())
       
    gl = gitlab.Gitlab('http://139.162.7.171/', private_token='BfHfZr1eLX1uRUKSD_pu')  #note token expire 30 Apr 
    
    groups = gl.groups.list()
    projects = gl.projects.list()
        
    all_projects = gl.projects.list(all=True)
    all_groups=gl.groups.list(all=True)
    length=len(all_projects)
    if prjlen == 0:
        i=0
        NewmemberCnt=0
        report['Project'] = []
    
        while i < length:
            project = gl.projects.get(all_projects[i].id)
            
            statistics = project.additionalstatistics.get()
            total_fetches = project.additionalstatistics.get().fetches['total']
            days_fetches = project.additionalstatistics.get().fetches['days']
            # print(days_fetches)
            
            #if project.created_at >= ('2021-03-01')
            #update 17 Mar 
            NewCnt = CalAge(project.created_at)
            # print(NewCnt)
            if NewCnt < 20:
                NewmemberCnt = NewmemberCnt+1
                # print(">>>>" , NewmemberCnt)

            # thelink = "<a href='"
            # thelink += str(project.web_url)
            # thelink += "'>"
            # thelink += str(project.web_url)
            # thelink += "</a>"

            report['Project'].append({
            'ID': project.id,
            'name': project.name,
            'StartDate': project.created_at,
            'Age': CalAge(project.created_at),
            'NoMember': len(project.users.list()),
            'total': total_fetches,
            'day': days_fetches,
            'URL Download': project.http_url_to_repo #project.web.url
            })

            i=i+1


        with open('report.json', 'w') as outfile:
            json.dump(report, outfile)
        
        f = open("static/thereport.html", "w")
        f.write(json2html.convert(json = report))
        f.close()

        
    topics['totalproject'] = i
    #update 17 Mar
    topics['newproject'] = NewmemberCnt

    users = gl.users.list()
    noUser=len(users)
    topics['member'] = noUser


    return render_template("home.html",topics=topics)
    
@app.route("/about")
def about():
    return render_template("about.html")

@app.route('/login')
def login():
    return render_template("login.html")


@app.route('/report', methods=['GET'])
# @no_cache
def serve_dir_directory_index():
    # f = open("static/thereport.html", "w")
    # f.write(json2html.convert(json = report))
    return send_from_directory(static_file_dir, 'thereport.html')

#https://roytuts.com/how-to-download-file-using-python-flask/
@app.route('/download')
def download_file():
    funcZIP()
    path = ".\\download.zip"
    # for LINUX
    # path = "./download.zip"
    return send_file(path, as_attachment=True)

@app.route('/upload')
def upload_file():
#    funcZIP()
   return render_template('upload.html')
	
@app.route('/uploader', methods = ['GET', 'POST'])
def uploade_file():
    if request.method == 'POST':
        f = request.files['file']
        print(f)
        os.makedirs(os.path.join(app.instance_path, 'upload'), exist_ok=True)
        # change_permissions_recursive('upload',0o777 )
        
        f.save(os.path.join(app.instance_path, 'upload', secure_filename(f.filename)))
       

    return 'file uploaded successfully'

@app.after_request
def add_header(request):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    request.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    request.headers["Pragma"] = "no-cache"
    request.headers["Expires"] = "0"
    request.headers['Cache-Control'] = 'public, max-age=0'
    return request



if __name__ == '__main__':
   app.run(debug=True,host="0.0.0.0")
   #scheduler.add_job(id = 'Scheduled task' , func =home , trigger = 'interval' , second =10)
   #scheduler.start()